import { computed, nextTick, onMounted, reactive, ref } from 'vue'
import { defineStore } from 'pinia'
import type { Material } from '@/types/Material'
import type { VForm } from 'vuetify/components'

export const useMaterialStore = defineStore('Material', () => {
  const dialog = ref(false)
  const form = ref(false)
  const loading = ref(false)
  const dialogDelete = ref(false)
  const stock = ref<Material[]>([
    { id: 1, name: 'ผงกาแฟ', status: 'Available', min: 1, balance: 10, unit: 'Kilogram' },
    { id: 2, name: 'เมล็ดกาแฟ', status: 'Available', min: 2, balance: 10, unit: 'Kilogram' },
    { id: 3, name: 'นม', status: 'Low', min: 3, balance: 2, unit: 'Liter' },
    { id: 4, name: 'มะนาว', status: 'Available', min: 2, balance: 4, unit: 'Kilogram' },
    { id: 5, name: 'นํ้าเชื่อม', status: 'Available', min: 1, balance: 2, unit: 'Liter' },
    { id: 6, name: 'ผงชาเขียว', status: 'Low', min: 3, balance: 2, unit: 'Kilogram' },
    { id: 7, name: 'ผงชา', status: 'Available', min: 2, balance: 2, unit: 'Kilogram' },
    { id: 8, name: 'แก้วพลาสติก', status: 'Available', min: 10, balance: 12, unit: 'piece' },
    { id: 9, name: 'หลอด', status: 'Available', min: 9, balance: 12, unit: 'piece' },
    { id: 10, name: 'ครีมเทียม', status: 'Available', min: 2, balance: 3, unit: 'Kilogram' },
    { id: 11, name: 'นํ้าผึ้ง', status: 'Low', min: 2, balance: 1, unit: 'Liter' },
    { id: 12, name: 'นมข้น', status: 'Available', min: 1, balance: 2, unit: 'Liter' }
  ])

  const totalMaterials = computed(() => material.value.length);
const totalAvailableMaterials = computed(() => {
  return combinedMaterials.value.filter(item => item.status === 'Available').length;
});
const totalLowMaterials = computed(() => {
  return combinedMaterials.value.filter(item => item.status === 'Low').length;
});
const totalOutMaterials = computed(() => {
  return combinedMaterials.value.filter(item => item.status === 'Out of Stock').length;
});
const headers = [
  {
    title: 'Material ID',
    key: 'id',
    value: 'id'
  }, {
    title: 'Material Name',
    key: 'name',
    value: 'name'
  },{
    title: 'Min',
    key: 'min',
    value: 'min'
  }, {
    title: 'Balance',
    key: 'balance',
    value: 'balance'
  }, {
    title: 'Unit',
    key: 'unit',
    value: 'unit'
  }, {
    title: 'Status(auto)',
    key: 'status',
    value: 'status'
  },
  {
    title: 'Actions', key: 'actions', sortable: false
  }
]

  const initilMaterial: Material = {
    id: -1,
    name: '',
    status: 'Available',
    min: 0,
    balance: 0,
    unit: ''
  }
  type status = 'Available' | 'Low' | 'Out of Stock'
  const editedMaterial = ref<Material>(JSON.parse(JSON.stringify(initilMaterial)))
  const material = ref<Material[]>([])
  const refForm = ref<VForm | null>(null)
  function onSubmit() {}
  let editedIndex = -1
  let lastId = 13
  const search = ref<string>('')
  onMounted(() => {
    stock
  })
  function closeDialog() {
    dialog.value = false
    nextTick(() => {
      editedMaterial.value = Object.assign({}, initilMaterial)
      editedIndex = -1
    })
  }
  async function save() {
    const { valid } = await refForm.value!.validate()

    if (!valid) return

    if (editedIndex > -1) {
      // Update existing item at the same index
      const updatedItem = { ...editedMaterial.value }
      material.value.splice(editedIndex, 1, updatedItem)
      console.log('edit')
    } else {
      // Add new item with a new ID
      editedMaterial.value.id = lastId++
      material.value.push(editedMaterial.value)
      console.log('add')
    }

    closeDialog()
  }
  const combinedMaterials = computed(() => {
    const searchTerm = search.value.toLowerCase()
    return material.value
      .map((item) => {
        let updatedStatus = ''
        if (item.balance >= item.min) {
          updatedStatus = 'Available'
        } else if (item.balance == 0) {
          updatedStatus = 'Out of Stock'
        } else if (item.balance < item.min) {
          updatedStatus = 'Low'
        }
        return { ...item, status: updatedStatus }
      })
      .filter((item) => {
        return (
          item.name.toLowerCase().includes(searchTerm) ||
          item.status.toLowerCase().includes(searchTerm)
        )
      })
  })
  function editItem(item: Material) {
    editedIndex = material.value.findIndex((m) => m.id === item.id)
    editedMaterial.value = reactive({ ...item })
    dialog.value = true
  }
  function closeDelete() {
    dialogDelete.value = false
    nextTick(() => {
      editedMaterial.value = Object.assign({}, initilMaterial)
    })
  }
  function deleteItemConfirm(materials: Material) {
    const index = material.value.findIndex((item) => item === materials)
    material.value.splice(index, 1)
    closeDelete()
  }
  function deleteItem(item: Material) {
    editedIndex = material.value.indexOf(item)
    editedMaterial.value = reactive({ ...item })
    dialogDelete.value = true
  }

  return {
    dialog,
    form,
    loading,
    dialogDelete,
    stock,
    editedMaterial,
    material,
    initilMaterial,
    combinedMaterials,
    search,
    totalOutMaterials,
    totalMaterials,
    totalAvailableMaterials,
    totalLowMaterials,
    headers,
    onMounted,
    deleteItem,
    deleteItemConfirm,
    editItem,
    save,
    closeDialog,
    closeDelete,
    onSubmit
  }
})
