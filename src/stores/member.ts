import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Member } from '@/types/Member'

export const useMemberStore = defineStore('member', () => {
    const tel = ref('')
    const addNewMemberDialog = ref(false)
    const useMemberDialog = ref(false)
    const dialogConfirm = ref(false)
    const members = ref<Member[]>([
        { id: 1, name: 'มานะ รักชาติ', tel: '0881234567', point: 6 },
        { id: 2, name: 'มาดี มีใจ', tel: '088', point: 10 }

    ])
    const lastId = ref<number>(3)
    const editedMember = ref<Member>({
        id: -1,
        name: '',
        tel: '',
        point: 0
    })
    const currentMember = ref<Member | null>()
    // behind the parameter in of function is what it will return
    const searchMember = (tel: string) => {
        const index = members.value.findIndex((item) => item.tel === tel)
        if (index < 0) {
            currentMember.value = null
        }
        currentMember.value = members.value[index]
    }
    const clear = () => {
        currentMember.value = null
    }
    const clearTextField = () => {
        if (tel.value !== undefined && tel.value !== null) {
            tel.value = ''; // this value is holding input
        }
        console.log(tel.value)
    };
    const cancel = () => {
        addNewMemberDialog.value = false
    }
    const save = () => {
        // "..." thing will copy new object and push to members
        const newMember = { ...editedMember.value, id: lastId.value++ };
        members.value.push(newMember)
        addNewMemberDialog.value = false
        // clear value
        editedMember.value = {
            id: -1,
            name: '',
            tel: '',
            point: 0
        }
    }
    return {
        members, currentMember, tel, editedMember, addNewMemberDialog, useMemberDialog, dialogConfirm,
        searchMember, clear, clearTextField, cancel, save
    }
})
